package com.fanghuaiming.dtx.icbc.spi.aboc;

import com.fanghuaiming.dtx.common.model.aboc.AbocAccountInfo;
import com.fanghuaiming.dtx.icbc.spi.aboc.fallback.DtxAbocFeignClientFallBack;
import org.dromara.hmily.annotation.Hmily;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/****
 * @description: aboc微服务SPI
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午5:19
 *
 */
@FeignClient(name = "dtx-tcc-hmily-cloud-aboc",fallback = DtxAbocFeignClientFallBack.class)
public interface DtxAbocFeignClient {

    /**
     * @Description: 远程调用ABOC进行转账
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 上午12:06
     * @version:1.0.0
     */
    @Hmily
    @PostMapping("/aboc/updateAbocAccountBalance")
    String updateAbocAccountBalance(AbocAccountInfo abocAccountInfo);
}
