package com.fanghuaiming.dtx.icbc.dao.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/****
 * @description: ICBC实体类
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/12/2 下午4:40
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IcbcOrderInfo implements Serializable {

    /**
     * 主键
     */
    private Long id;

    /**
     * 账户名
     */
    private String accountName;

    /**
     * 账户编号
     */
    private String accountNo;

    /**
     * 账户密码
     */
    private String accountPassword;

    /**
     * 账户余额/转账金额
     */
    private Double accountBalance;
}
