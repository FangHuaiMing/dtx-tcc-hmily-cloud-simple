package com.fanghuaiming.dtx.icbc.spi.aboc.fallback;

import com.fanghuaiming.dtx.common.model.aboc.AbocAccountInfo;
import com.fanghuaiming.dtx.common.model.enumeration.ResultEnum;
import com.fanghuaiming.dtx.icbc.spi.aboc.DtxAbocFeignClient;
import org.springframework.stereotype.Component;

/****
 * @description: aboc微服务熔断降级
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午6:01
 *
 */
@Component
public class DtxAbocFeignClientFallBack implements DtxAbocFeignClient {

    /**
     * @Description: 降级熔断
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 上午12:07
     * @version:1.0.0
     */
    @Override
    public String updateAbocAccountBalance(AbocAccountInfo abocAccountInfo) {
        return ResultEnum.FALL_BACK.getMessage();
    }
}
