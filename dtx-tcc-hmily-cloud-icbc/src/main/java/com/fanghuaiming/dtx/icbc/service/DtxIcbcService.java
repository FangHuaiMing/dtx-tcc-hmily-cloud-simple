package com.fanghuaiming.dtx.icbc.service;

import com.fanghuaiming.dtx.icbc.dao.model.IcbcOrderInfo;

/****
 * @description: ICBC服务接口
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午1:58
 *
 */
public interface DtxIcbcService {

    /**
     * @Description: ICBC转账服务
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:52
     * @version:1.0.0
     */
    void transferIcbcAccount(IcbcOrderInfo icbcOrderInfo);
}
