package com.fanghuaiming.dtx.icbc.service.impl;

import com.fanghuaiming.dtx.icbc.dao.mapper.IcbcOrderInfoMapper;
import com.fanghuaiming.dtx.icbc.dao.model.IcbcOrderInfo;
import com.fanghuaiming.dtx.icbc.service.DtxIcbcService;
import lombok.extern.slf4j.Slf4j;
import org.dromara.hmily.annotation.Hmily;
import org.dromara.hmily.annotation.PatternEnum;
import org.dromara.hmily.core.concurrent.threadlocal.HmilyTransactionContextLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/****
 * @description: ICBC服务接口实现类
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午1:59
 *
 */
@Service
@Slf4j
public class DtxIcbcServiceImpl implements DtxIcbcService {

    /**
     * ICBC的mapper
     */
    @Autowired
    private IcbcOrderInfoMapper icbcOrderInfoMapper;


    /**
     * @Description: TCC-第一阶段Try
     *  转账ICBC业务:
     *              空预处理
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:53
     * @version:1.0.0
     */
    @Override
    @Hmily(confirmMethod = "commitIcbcAccount",cancelMethod = "rollbackIcbcAccount",pattern = PatternEnum.TCC)
    public void transferIcbcAccount(IcbcOrderInfo icbcOrderInfo) {
        //获取全局事务id
        String transactionId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        log.info("========== TCC-第一阶段Try confirm空预处理【transferIcbcAccount】,全局事务xid:{} ==========",transactionId);
    }

    /**
     * @Description: TCC-第二阶段Confirm
     *
     *              confirm幂等校验
     *              正式增加指定金额
     *
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:55
     * @version:1.0.0
     */
    @SuppressWarnings("all")
    @Transactional
    public void commitIcbcAccount(IcbcOrderInfo icbcOrderInfo) {
        //获取全局事务id
        String transactionId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        log.info("========== TCC-第二阶段Confirm=执行【commitIcbcAccount】进行ICBC账户转账,全局事务xid:{} ==========",transactionId);
        if(icbcOrderInfoMapper.isExistConfirm(transactionId)>0){
            log.info("========== TCC-第二阶段 confirm已被执行过 全局事务xid:{},请勿重复执行 ==========",transactionId);
            return ;
        }
        //增加金额
        icbcOrderInfoMapper.addIcbcAccountBalance(icbcOrderInfo.getAccountNo(),icbcOrderInfo.getAccountBalance());
        //增加一条confirm日志，用于幂等
        icbcOrderInfoMapper.addConfirm(transactionId);
        log.info("========== 已经完成!!! TCC-第二阶段Confirm=执行【commitIcbcAccount】进行ICBC账户转账,全局事务xid:{} ==========",transactionId);
    }

    /**
     * @Description: TCC-第二阶段Cancel
     *
     *          空回滚
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:55
     * @version:1.0.0
     */
    @SuppressWarnings("all")
    @Transactional
    public void rollbackIcbcAccount(IcbcOrderInfo icbcOrderInfo) {
        //获取全局事务id
        String transactionId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        log.info("========== TCC-第二阶段Cancel cancel空回滚【rollbackIcbcAccount】,全局事务xid:{} ==========",transactionId);
    }


}
