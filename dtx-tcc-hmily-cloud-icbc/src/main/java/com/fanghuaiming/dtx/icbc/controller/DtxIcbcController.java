package com.fanghuaiming.dtx.icbc.controller;

import com.fanghuaiming.dtx.icbc.dao.model.IcbcOrderInfo;
import com.fanghuaiming.dtx.icbc.service.DtxIcbcService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/****
 * @description: ICBC服务接口
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午2:00
 *
 */
@Slf4j
@RestController("icbc")
@RequestMapping("/icbc")
public class DtxIcbcController {


    /**
     * ICBC服务
     */
    @Autowired
    private DtxIcbcService dtxIcbcService;

    /**
     * @Description: 转账ICBC金额业务
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:51
     * @version:1.0.0
     */
    @PostMapping("/transferIcbcAccount")
    public Boolean transferIcbcAccount(@RequestBody IcbcOrderInfo icbcOrderInfo) {
        dtxIcbcService.transferIcbcAccount(icbcOrderInfo);
        return true;
    }
}
