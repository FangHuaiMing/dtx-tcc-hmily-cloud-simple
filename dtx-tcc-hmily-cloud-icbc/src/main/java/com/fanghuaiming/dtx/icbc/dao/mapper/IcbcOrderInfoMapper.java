package com.fanghuaiming.dtx.icbc.dao.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/****
 * @description: 支付dao操作接口
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午1:57
 *
 */
@Repository
public interface IcbcOrderInfoMapper {

    /**
     * @Description: 扣减账户余额
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:43
     * @version:1.0.0
     */
    int subtractIcbcAccountBalance(@Param("accountNo") String accountNo, @Param("accountBalance") Double accountBalance);

    /**
     * @Description: 添加账户余额
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:43
     * @version:1.0.0
     */
    int addIcbcAccountBalance(@Param("accountNo") String accountNo, @Param("accountBalance") Double accountBalance);

    /**
     * @Description: 增加某分支事务try执行记录
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:43
     * @version:1.0.0
     */
    int addTry(@Param("txNo") String txNo);

    /**
     * @Description: 增加某分支事务confirm执行记录
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:44
     * @version:1.0.0
     */
    int addConfirm(@Param("txNo") String txNo);

    /**
     * @Description: 增加某分支事务cancel执行记录
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:44
     * @version:1.0.0
     */
    int addCancel(@Param("txNo") String txNo);

    /**
     * @Description: 查询分支事务try是否已执行
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:45
     * @version:1.0.0
     */
    int isExistTry(@Param("txNo") String txNo);

    /**
     * @Description: 查询分支事务confirm是否已执行
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:45
     * @version:1.0.0
     */
    int isExistConfirm(@Param("txNo") String txNo);

    /**
     * @Description: 查询分支事务cancel是否已执行
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:45
     * @version:1.0.0
     */
    int isExistCancel(@Param("txNo") String txNo);

}
