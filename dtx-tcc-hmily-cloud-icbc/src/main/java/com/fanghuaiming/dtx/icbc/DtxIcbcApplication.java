package com.fanghuaiming.dtx.icbc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/****
 * @description: 账户微服务启动类
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 上午11:30
 *
 */
@EnableTransactionManagement
@SpringBootApplication(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
@MapperScan(value = {"com.fanghuaiming.dtx.icbc.dao.mapper"})
/**
 * 扫描到hmily使用的注解
 */
@ComponentScan(basePackages = {"com.fanghuaiming.dtx.icbc.**","org.dromara.hmily.*"})
@EnableFeignClients
/**
 * Hmily基于Aop作用
 */
@EnableAspectJAutoProxy(exposeProxy = true)
public class DtxIcbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(DtxIcbcApplication.class,args);
    }
}
