package com.fanghuaiming.dtx.icbc.config;

import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

/****
 * @description: ICBC数据源
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/28 下午8:44
 *
 */
@Configuration
@MapperScan(basePackages = DtxIcbcDataSourceConfig.PACKAGE, sqlSessionFactoryRef = "dtxIcbcSqlSessionFactory")
public class DtxIcbcDataSourceConfig {

    static final String PACKAGE = "com.fanghuaiming.dtx.icbc.dao.mapper";


    /**
     * @Description: 定义dataSource
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/1 下午1:30
     * @version:1.0.0
     */
    @Bean(name = "dtxIcbcDataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource.dtx.icbc")
    public HikariDataSource dtxIcbcDataSource() {
        return new HikariDataSource();
    }

    /**
     * @Description: 定义TransactionManager
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/1 下午1:31
     * @version:1.0.0
     */
    @Bean(name = "dtxIcbcTransactionManager")
    @Primary
    public DataSourceTransactionManager dtxIcbcTransactionManager() {
        return new DataSourceTransactionManager(this.dtxIcbcDataSource());
    }

    /**
     * @Description: 定义SqlSessionFactory
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/1 下午1:31
     * @version:1.0.0
     */
    @Bean(name = "dtxIcbcSqlSessionFactory")
    @Primary
    public SqlSessionFactory dtxIcbcSqlSessionFactory(@Qualifier("dtxIcbcDataSource") DataSource dataSource) throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setMapperLocations(
                new PathMatchingResourcePatternResolver().getResources("classpath:mappers/*.xml"));
        sessionFactory.getObject().getConfiguration().setMapUnderscoreToCamelCase(true);
        return sessionFactory.getObject();
    }

}
