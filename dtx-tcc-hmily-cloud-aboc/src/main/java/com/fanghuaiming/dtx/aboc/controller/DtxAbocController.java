package com.fanghuaiming.dtx.aboc.controller;

import com.fanghuaiming.dtx.aboc.dao.model.AbocAccountInfo;
import com.fanghuaiming.dtx.aboc.service.DtxAbocService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/****
 * @description: ABOC账户服务接口
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午2:00
 *
 */
@RestController("aboc")
@RequestMapping("/aboc")
@Slf4j
public class DtxAbocController {

    /**
     * ABOC账户服务
     */
    @Autowired
    private DtxAbocService dtxAbocService;

    /**
     * @Description: ABOC转账业务
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:47
     * @version:1.0.0
     */
    @PostMapping("/updateAbocAccountBalance")
    public String updateAbocAccountBalance(@RequestBody AbocAccountInfo abocAccountInfo) {
        dtxAbocService.updateAbocAccountBalance(abocAccountInfo);
        return "转账成功,请与对方确认";
    }
}
