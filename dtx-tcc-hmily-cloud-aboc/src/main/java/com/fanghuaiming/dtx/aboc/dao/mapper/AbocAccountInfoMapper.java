package com.fanghuaiming.dtx.aboc.dao.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/****
 * @description: 订单dao操作接口
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午1:57
 *
 */
@Repository
public interface AbocAccountInfoMapper {

    /**
     * @Description: 扣减账户余额
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午9:30
     * @version:1.0.0
     */
    int subtractAbocAccountBalance(@Param("accountNo") String accountNo, @Param("accountBalance") Double accountBalance);

    /**
     * @Description: 添加账户余额
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午9:32
     * @version:1.0.0
     */
    int addAbocAccountBalance(@Param("accountNo") String accountNo, @Param("accountBalance") Double accountBalance);

    /**
     * @Description: 增加某分支事务try执行记录
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午9:32
     * @version:1.0.0
     */
    int addTry(@Param("txNo") String txNo);

    /**
     * @Description: 增加某分支事务confirm执行记录
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午9:33
     * @version:1.0.0
     */
    int addConfirm(@Param("txNo") String txNo);

    /**
     * @Description: 增加某分支事务cancel执行记录
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午9:33
     * @version:1.0.0
     */
    int addCancel(@Param("txNo") String txNo);

    /**
     * @Description: 查询分支事务try是否已执行
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午9:33
     * @version:1.0.0
     */
    int isExistTry(@Param("txNo") String txNo);

    /**
     * @Description: 查询分支事务confirm是否已执行
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午9:33
     * @version:1.0.0
     */
    int isExistConfirm(@Param("txNo") String txNo);

    /**
     * @Description: 查询分支事务cancel是否已执行
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午9:33
     * @version:1.0.0
     */
    int isExistCancel(@Param("txNo") String txNo);

}
