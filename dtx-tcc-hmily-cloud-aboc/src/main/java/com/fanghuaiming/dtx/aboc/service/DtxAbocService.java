package com.fanghuaiming.dtx.aboc.service;

import com.fanghuaiming.dtx.aboc.dao.model.AbocAccountInfo;

/****
 * @description: aboc服务接口
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午1:58
 *
 */
public interface DtxAbocService {

    //账户扣款
    /**
     * @Description: ABOC账户扣款
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午10:28
     * @version:1.0.0
     */
    public  void updateAbocAccountBalance(AbocAccountInfo abocAccountInfo);

}
