package com.fanghuaiming.dtx.aboc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/****
 * @description: 订单微服务启动类
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午2:38
 *
 */
@EnableTransactionManagement
@SpringBootApplication(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
@MapperScan(value = {"com.fanghuaiming.dtx.aboc.dao.mapper"})
/**
 * 扫描到hmily使用的注解
 */
@ComponentScan(basePackages = {"com.fanghuaiming.dtx.aboc.**","org.dromara.hmily.*"})
@EnableFeignClients
/**
 * Hmily基于Aop作用
 */
@EnableAspectJAutoProxy(exposeProxy = true)
public class DtxAbocApplication {

    public static void main(String[] args) {
        SpringApplication.run(DtxAbocApplication.class,args);
    }
}
