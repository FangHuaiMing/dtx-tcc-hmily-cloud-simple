package com.fanghuaiming.dtx.aboc.spi.icbc;

import com.fanghuaiming.dtx.aboc.spi.icbc.fallback.DtxIcbcFeignClientFallBack;
import com.fanghuaiming.dtx.common.model.icbc.IcbcOrderInfo;
import org.dromara.hmily.annotation.Hmily;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/****
 * @description: ICBC微服务SPI
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午5:19
 *
 */
@FeignClient(name = "dtx-tcc-hmily-cloud-icbc",fallback = DtxIcbcFeignClientFallBack.class)
public interface DtxIcbcFeignClient {

    /**
     * @Description: 远程调用ICBC进行转账
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:23
     * @version:1.0.0
     */
    @Hmily
    @PostMapping("/icbc/transferIcbcAccount")
    boolean transferIcbcAccount(IcbcOrderInfo icbcOrderInfo);
}
