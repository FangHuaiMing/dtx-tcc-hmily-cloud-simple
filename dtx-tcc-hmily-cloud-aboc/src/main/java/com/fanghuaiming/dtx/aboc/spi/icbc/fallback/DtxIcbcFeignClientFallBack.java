package com.fanghuaiming.dtx.aboc.spi.icbc.fallback;

import com.fanghuaiming.dtx.aboc.spi.icbc.DtxIcbcFeignClient;
import com.fanghuaiming.dtx.common.model.icbc.IcbcOrderInfo;
import org.springframework.stereotype.Component;

/****
 * @description: ICBC微服务熔断降级
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午6:01
 *
 */
@Component
public class DtxIcbcFeignClientFallBack implements DtxIcbcFeignClient {

    /**
     * @Description: 降级熔断方法
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/2 下午11:49
     * @version:1.0.0
     */
    @Override
    public boolean transferIcbcAccount(IcbcOrderInfo icbcOrderInfo) {
        return false;
    }
}
