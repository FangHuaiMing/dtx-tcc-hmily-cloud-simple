package com.fanghuaiming.dtx.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/****
 * @description: eureka服务管理
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午3:49
 *
 */
@SpringBootApplication
@EnableEurekaServer
public class DtxEurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DtxEurekaServerApplication.class,args);
    }
}
