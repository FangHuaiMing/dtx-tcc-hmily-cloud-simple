package com.fanghuaiming.dtx.feign.spi.aboc;

import org.springframework.cloud.openfeign.FeignClient;

/****
 * @description: 订单微服务SPI
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午5:22
 *
 */
@FeignClient(name = "dtx-tcc-hmily-cloud-aboc")
public interface DtxAbocFeignClient {


}
