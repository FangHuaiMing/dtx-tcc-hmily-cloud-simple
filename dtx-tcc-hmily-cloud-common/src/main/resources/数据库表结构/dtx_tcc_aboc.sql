/*
 Navicat Premium Data Transfer

 Source Server         : localhost3308
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : localhost:3308
 Source Schema         : dtx_tcc_aboc

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 02/12/2020 23:37:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for aboc_account_info
-- ----------------------------
DROP TABLE IF EXISTS `aboc_account_info`;
CREATE TABLE `aboc_account_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '户主姓名',
  `account_no` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '银行卡号',
  `account_password` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '帐户密码',
  `account_balance` double DEFAULT NULL COMMENT '帐户余额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='农业银行账户';

-- ----------------------------
-- Records of aboc_account_info
-- ----------------------------
BEGIN;
INSERT INTO `aboc_account_info` VALUES (1, '张三', '1', NULL, 1000);
COMMIT;

-- ----------------------------
-- Table structure for local_cancel_log
-- ----------------------------
DROP TABLE IF EXISTS `local_cancel_log`;
CREATE TABLE `local_cancel_log` (
  `tx_no` varchar(64) NOT NULL COMMENT '事务id',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`tx_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for local_confirm_log
-- ----------------------------
DROP TABLE IF EXISTS `local_confirm_log`;
CREATE TABLE `local_confirm_log` (
  `tx_no` varchar(64) NOT NULL COMMENT '事务id',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`tx_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for local_try_log
-- ----------------------------
DROP TABLE IF EXISTS `local_try_log`;
CREATE TABLE `local_try_log` (
  `tx_no` varchar(64) NOT NULL COMMENT '事务id',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`tx_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
