/*
 Navicat Premium Data Transfer

 Source Server         : localhost3309
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : localhost:3309
 Source Schema         : dtx_tcc_icbc

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 02/12/2020 23:37:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for icbc_account_info
-- ----------------------------
DROP TABLE IF EXISTS `icbc_account_info`;
CREATE TABLE `icbc_account_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '户主姓名',
  `account_no` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '银行卡号',
  `account_password` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '帐户密码',
  `account_balance` double DEFAULT NULL COMMENT '帐户余额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='工商银行';

-- ----------------------------
-- Records of icbc_account_info
-- ----------------------------
BEGIN;
INSERT INTO `icbc_account_info` VALUES (3, '李四', '2', NULL, 0);
COMMIT;

-- ----------------------------
-- Table structure for local_cancel_log
-- ----------------------------
DROP TABLE IF EXISTS `local_cancel_log`;
CREATE TABLE `local_cancel_log` (
  `tx_no` varchar(64) NOT NULL COMMENT '事务id',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`tx_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for local_confirm_log
-- ----------------------------
DROP TABLE IF EXISTS `local_confirm_log`;
CREATE TABLE `local_confirm_log` (
  `tx_no` varchar(64) NOT NULL COMMENT '事务id',
  `create_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for local_try_log
-- ----------------------------
DROP TABLE IF EXISTS `local_try_log`;
CREATE TABLE `local_try_log` (
  `tx_no` varchar(64) NOT NULL COMMENT '事务id',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`tx_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
